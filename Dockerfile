FROM ubuntu:19.10

# Add a timestamp for the build. Also, bust the cache.
ADD http://worldclockapi.com/api/json/utc/now /opt/docker/etc/timestamp

# Install system packages
RUN apt-get update --yes -qq \
    && DEBIAN_FRONTEND=noninteractive apt-get install --yes -o Dpkg::Options::="--force-confnew" -qq \
            apt-transport-https \
            ca-certificates \
            gnupg \
            software-properties-common \
            wget \
    && wget -qO - https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2023.PUB | apt-key add - \
    && apt-add-repository -y "deb https://apt.repos.intel.com/oneapi all main" \
    && apt-get update --yes -qq \
    && apt-get upgrade --yes -qq \
    && DEBIAN_FRONTEND=noninteractive apt-get install --yes -o Dpkg::Options::="--force-confnew" -qq \
            build-essential \
            curl \
            g++ \
            gcc \
            gfortran \
            git \
            intel-oneapi-icc \
            intel-oneapi-ifort \
            intel-oneapi-mkl \
            intel-oneapi-mkl-devel \
            intel-oneapi-mpi \
            intel-oneapi-mpi-devel \
            intel-oneapi-openmp \
            libboost-all-dev \
            libffi-dev \
            libopenmpi-dev \
            libpython3-dev \
            openmpi-bin \
            python3 \
            python3-pip \
            zlib1g-dev \
    && apt-get clean --yes -qq \
    && rm -rf /var/lib/apt/lists/* \
    # Install CMake
    && mkdir -p /opt/cmake \
    && curl -Ls https://cmake.org/files/v3.14/cmake-3.14.7-Linux-x86_64.tar.gz | tar -xz -C /opt/cmake --strip-components=1 \
    # Install Python packages globally
    && pip3 install --upgrade \
            cffi \
            numpy \
            pytest \
            pytest-datadir \
            runtest \
            scipy

COPY intel-entrypoint.sh /opt/intel-entrypoint.sh
COPY gnu-entrypoint.sh /opt/gnu-entrypoint.sh
ENTRYPOINT ["/opt/gnu-entrypoint.sh"]

CMD [ "/bin/bash" ]
