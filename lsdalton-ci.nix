{ system ? builtins.currentSystem }:

let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs { inherit system; };
  pythonEnv = import ./nix/pythonEnv.nix { inherit sources; };
in
pkgs.dockerTools.buildImage {
  name = "registry.gitlab.com/robertodr/lsdalton-ci";
  created = "now";
  fromImage = ./ubuntu.tar;
  contents = with pkgs; [
    bash
    binutils-unwrapped
    boost.dev
    busybox
    cmake
    coreutils
    gcc
    gfortran
    git
    gnumake
    libffi.dev
    mkl
    ninja
    openmpi
    pythonEnv
    utillinux
    zlib.dev
  ];
  config = {
    Cmd = [ "${pkgs.bash}" ];
    Env = [
      "OMPI_ALLOW_RUN_AS_ROOT=1"
      "OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1"
      "OMPI_MCA_plm_rsh_agent="""
      "MATH_ROOT=${pkgs.mkl.outPath}"
      "ZLIB_ROOT=${pkgs.zlib.outPath}"
      "NINJA_STATUS='[Built edge %f of %t in %e sec] '"
    ];
  };
}
