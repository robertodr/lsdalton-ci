#!/bin/bash

. /opt/intel/inteloneapi/setvars.sh

export MATH_ROOT="$MKLROOT"
export PATH=/opt/cmake/bin:"$PATH"

exec "$@"
