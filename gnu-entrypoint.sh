#!/bin/bash

. /opt/intel/inteloneapi/mkl/latest/env/vars.sh

export MATH_ROOT="$MKLROOT"
export PATH=/opt/cmake/bin:"$PATH"

exec "$@"
